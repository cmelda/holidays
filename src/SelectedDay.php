<?php

declare(strict_types=1);

namespace Cmelda\Holidays;

use Cmelda\Holidays\Exceptions\NoNameException;
use Cmelda\Holidays\Exceptions\NoPublicHolidayException;
use DateTimeImmutable;

final readonly class SelectedDay
{
	/** @var string[] */
	protected array $names;

	/** @var string[] */
	protected ?array $specialHoliday;
	protected DateTimeImmutable $date;

	public function __construct(DateTimeImmutable $date)
	{
		$this->date = $date;
	}

	/**
	 * @internal
	 * @param	 string[] $names
	 */
	public function setNames(array $names): static
	{
		$this->names = $names;

		return $this;
	}

	/**
	 * @internal
	 * @param	 string[]|null $specialHoliday
	 */
	public function setPublicHolidays(?array $specialHoliday): static
	{
		$this->specialHoliday = $specialHoliday;

		return $this;
	}

	/**
	 * @return string[]
	 * @throws NoNameException if name is not exists
	 */
	public function getNames(): array
	{
		if (count($this->names) > 0) {
			return $this->names;
		}

		throw new NoNameException($this->date);
	}

	/**
	 * @return string[]
	 * @throws NoPublicHolidayException if public holiday is not exists
	 */
	public function getPublicHolidays(): array
	{
		if ($this->specialHoliday !== null) {
			return $this->specialHoliday;
		}

		throw new NoPublicHolidayException($this->date);
	}

	public function isPublicHoliday(): bool
	{
		try {
			$this->getPublicHolidays();
		} catch (NoPublicHolidayException) {
			return false;
		}

		return true;
	}
}
