<?php

declare(strict_types=1);

namespace Cmelda\Holidays;

use DateTimeInterface;

interface Special
{
	public function isSpecial(DateTimeInterface $date): bool;

	public function getName(): string;
}
