<?php

declare(strict_types=1);

namespace Cmelda\Holidays;

use Cmelda\Holidays\Exceptions\NoPublicHolidayException;
use DateTimeImmutable;
use DateTimeInterface;
use OutOfBoundsException;

final class Holidays
{
	protected readonly string $iso;

	/** @var array<string, string[]> */
	protected readonly array $data;

	/** @var Special[] */
	protected array $specialHolidays = [];

	/** @var string[] */
	protected array $translates = [];

	protected const DateFormat = 'j.n.';

	public function __construct(string $iso)
	{
		$this->iso = strtolower($iso);
		$country = Country::tryFrom($this->iso);

		if ($country === null) {
			throw new OutOfBoundsException('The country code was not recognized or implemented.');
		}

		$this->specialHolidays = $country->getSpecialHolidays();

		$file = __DIR__ . '/names/' . $this->iso . '.json';

		$json = file_get_contents($file);
		$this->data = json_decode($json ?: '', true) ?: [];
	}

	public function getDay(DateTimeInterface $date): SelectedDay
	{
		$date = DateTimeImmutable::createFromInterface($date);
		$day = new SelectedDay($date);

		$country = Country::from($this->iso);

		$newDateString = $country->getCountrySpecial()->changeDay($date)->format(self::DateFormat);

		$day->setNames($this->data[$newDateString] ?? []);

		try {
			$day->setPublicHolidays($this->getPublicHolidays($date));
		} catch (NoPublicHolidayException) {
			$day->setPublicHolidays(null);
		}

		return $day;
	}

	/**
	 * @return string[]
	 */
	protected function getPublicHolidays(DateTimeInterface $date): array
	{
		$publicHolidays = [];
		$this->prepareTransaltes();

		foreach ($this->specialHolidays as $special) {
			if ($special->isSpecial($date)) {
				$publicHolidays[]
					= $this->translates[$special->getName()] ?? $special->getName();
			}
		}

		if (!empty($publicHolidays)) {
			return $publicHolidays;
		}

		throw new NoPublicHolidayException($date);
	}

	protected function prepareTransaltes(): void
	{
		if (count($this->translates ?? []) === 0) {
			$file = __DIR__ . '/translates/' . $this->iso . '.json';
			$json = file_get_contents($file);

			/** @phpstan-ignore assign.propertyType */
			$this->translates = json_decode($json ?: '', true) ?: [];
		}
	}
}
