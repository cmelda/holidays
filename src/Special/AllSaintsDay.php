<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class AllSaintsDay extends OneDaySpecial
{
	protected string $day = '0111';

	public function getName(): string
	{
		return 'all.saints.day';
	}
}
