<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class Pentecost extends Easter
{
	protected int $days = 49;

	public function getName(): string
	{
		return 'easter.pentecost';
	}
}
