<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class PentecostMonday extends Easter
{
	protected int $days = 50;

	public function getName(): string
	{
		return 'easter.pentecost.monday';
	}
}
