<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

use Cmelda\Holidays\Special;
use DateTime;
use DateTimeInterface;

class AllSaints implements Special
{
	public function isSpecial(DateTimeInterface $date): bool
	{
		$begin = new DateTime($date->format('Y') . '-10-31');
		$end = new DateTime($date->format('Y') . '-11-6');

		if (
			$date->getTimestamp() >= $begin->getTimestamp() &&
			$date->getTimestamp() <= $end->getTimestamp() &&
			($date->format('N') === '6')
		) {
				return true;
		}

		return false;
	}

	public function getName(): string
	{
		return 'all.saints';
	}
}
