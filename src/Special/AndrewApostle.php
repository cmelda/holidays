<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class AndrewApostle extends OneDaySpecial
{
	protected string $day = '3011';

	public function getName(): string
	{
		return 'andrew.apostle';
	}
}
