<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class MidsummerEve extends MidsummerDay
{
	protected int $day = -1;

	public function getName(): string
	{
		return 'midsummer.eve';
	}
}
