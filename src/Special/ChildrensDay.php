<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class ChildrensDay extends OneDaySpecial
{
	protected string $day = '0106';

	public function getName(): string
	{
		return 'childrens.day';
	}
}
