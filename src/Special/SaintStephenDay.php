<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class SaintStephenDay extends OneDaySpecial
{
	protected string $day = '2612';

	public function getName(): string
	{
		return 'christmas.stephen';
	}
}
