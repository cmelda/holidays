<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class WorkersDayAfter extends OneDaySpecial
{
	protected string $day = '0205';

	public function getName(): string
	{
		return 'workers.day.after';
	}
}
