<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class CyrilMethodius extends OneDaySpecial
{
	protected string $day = '0507';

	public function getName(): string
	{
		return 'cyril.methodius';
	}
}
