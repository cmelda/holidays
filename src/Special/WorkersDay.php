<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class WorkersDay extends OneDaySpecial
{
	protected string $day = '0105';

	public function getName(): string
	{
		return 'workers.day';
	}
}
