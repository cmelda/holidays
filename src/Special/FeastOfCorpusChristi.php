<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

use DateTime;

class FeastOfCorpusChristi extends Easter
{
	protected int $days = 50;

	public function getName(): string
	{
		return 'easter.feast.of.corpus.christi';
	}

	protected function modifyDate(DateTime $date): void
	{
		$date->modify('next sunday')->modify('next thursday');
	}
}
