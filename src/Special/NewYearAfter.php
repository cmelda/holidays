<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class NewYearAfter extends OneDaySpecial
{
	protected string $day = '0201';

	public function getName(): string
	{
		return 'new.year.after';
	}
}
