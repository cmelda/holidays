<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class LadyOfSorrows extends OneDaySpecial
{
	protected string $day = '1509';

	public function getName(): string
	{
		return 'lady.sorrows';
	}
}
