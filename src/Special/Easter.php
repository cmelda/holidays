<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

use Cmelda\Holidays\Special;
use DateTime;
use DateTimeInterface;

class Easter implements Special
{
	protected int $days = 0;

	public function isSpecial(DateTimeInterface $date): bool
	{
		$easter = new DateTime();
		$easter->setTimestamp($this->getEasterDate($date));
		$easter->modify(sprintf('%d days', $this->days));
		$this->modifyDate($easter);

		$diff = $date->diff($easter);

		// @codingStandardsIgnoreLine
		if ($diff->format('%a') === '0') {
			return true;
		}

		return false;
	}

	public function getName(): string
	{
		return 'easter';
	}

	protected function modifyDate(DateTime $date): void
	{
		// for additional modification
	}

	private function getEasterDate(DateTimeInterface $date): int
	{
		$year = intval($date->format('Y'));

		if (function_exists('easter_date')) {
			return easter_date($year);
		}

		$a = $year % 19;
		$b = $year % 4;
		$c = $year % 7;
		$p = floor($year / 100.0);
		$q = floor((13 + 8 * $p) / 25.0);
		$m = intval(15 - $q + $p - floor($p / 4)) % 30;
		$n = intval(4 + $p - floor($p / 4)) % 7;
		$d = intval(19 * $a + $m) % 30;
		$e = intval(2 * $b + 4 * $c + 6 * $d + $n) % 7;
		$days = intval(22 + $d + $e);
		if (($d === 29) && ($e === 6)) {
			$de = mktime(0, 0, 0, 4, 19, $year);
		} elseif (($d === 28) && ($e === 6)) {
			$de = mktime(0, 0, 0, 4, 18, $year);
		} else {
			if ($days > 31) {
				$de = mktime(0, 0, 0, 4, $days - 31, $year);
			} else {
				$de = mktime(0, 0, 0, 3, $days, $year);
			}
		}

		return $de;
	}
}
