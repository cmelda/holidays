<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class ChristmasDay extends OneDaySpecial
{
	protected string $day = '2512';

	public function getName(): string
	{
		return 'christmas.day';
	}
}
