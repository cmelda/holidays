<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class EasterThursday extends Easter
{
	protected int $days = -3;

	public function getName(): string
	{
		return 'easter.thursday';
	}
}
