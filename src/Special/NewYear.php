<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class NewYear extends OneDaySpecial
{
	protected string $day = '0101';

	public function getName(): string
	{
		return 'new.year';
	}
}
