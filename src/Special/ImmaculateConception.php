<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class ImmaculateConception extends OneDaySpecial
{
	protected string $day = '0812';

	public function getName(): string
	{
		return 'immaculate.conception';
	}
}
