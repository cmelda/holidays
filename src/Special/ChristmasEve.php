<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class ChristmasEve extends OneDaySpecial
{
	protected string $day = '2412';

	public function getName(): string
	{
		return 'christmas.eve';
	}
}
