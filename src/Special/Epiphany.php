<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class Epiphany extends OneDaySpecial
{
	protected string $day = '0601';

	public function getName(): string
	{
		return 'epiphany';
	}
}
