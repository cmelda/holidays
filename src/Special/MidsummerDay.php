<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

use Cmelda\Holidays\Special;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;

class MidsummerDay implements Special
{
	protected int $day = 0;

	public function isSpecial(DateTimeInterface $date): bool
	{
		$date = DateTimeImmutable::createFromInterface($date)->modify(strval(-$this->day) . ' days');
		$begin = new DateTime($date->format('Y') . '-6-20');
		$end = new DateTime($date->format('Y') . '-6-26');

		if (
			$date->getTimestamp() >= $begin->getTimestamp() &&
			$date->getTimestamp() <= $end->getTimestamp() &&
			($date->format('N') === '6')
		) {
				return true;
		}

		return false;
	}

	public function getName(): string
	{
		return 'midsummer';
	}
}
