<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

use Cmelda\Holidays\Special;
use DateTimeImmutable;
use DateTimeInterface;

abstract class OneDaySpecial implements Special
{
	protected string $day = '';

	public function isSpecial(DateTimeInterface $date): bool
	{
		$date = DateTimeImmutable::createFromInterface($date);

		return $date->format('dm') === $this->getOneDayDate()->format('dm');
	}

	private function getOneDayDate(): DateTimeImmutable
	{
		return DateTimeImmutable::createFromFormat('dm', $this->day);
	}

	abstract public function getName(): string;
}
