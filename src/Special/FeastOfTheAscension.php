<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class FeastOfTheAscension extends Easter
{
	protected int $days = 39;

	public function getName(): string
	{
		return 'easter.feast.of.the.ascension';
	}
}
