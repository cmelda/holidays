<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special;

class DormitionOfMotherOfGod extends OneDaySpecial
{
	protected string $day = '1508';

	public function getName(): string
	{
		return 'dormition.mother.god';
	}
}
