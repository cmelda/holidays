<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special\Orthodox;

class Pentecost extends Easter
{
	protected int $days = 49;

	public function getName(): string
	{
		return 'orthodox.easter.pentecost';
	}
}
