<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special\Orthodox;

class EasterFriday extends Easter
{
	protected int $days = -2;

	public function getName(): string
	{
		return 'orthodox.easter.friday';
	}
}
