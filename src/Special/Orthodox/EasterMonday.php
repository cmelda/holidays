<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special\Orthodox;

class EasterMonday extends Easter
{
	protected int $days = 1;

	public function getName(): string
	{
		return 'orthodox.easter.monday';
	}
}
