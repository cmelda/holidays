<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Special\Orthodox;

use Cmelda\Holidays\Special;
use DateTime;
use DateTimeInterface;

class Easter implements Special
{
	protected int $days = 0;

	public function isSpecial(DateTimeInterface $date): bool
	{
		$easter = new DateTime();
		$easter->setTimestamp($this->getEasterDate($date));
		$easter->modify(sprintf('%d days', $this->days));
		$this->modifyDate($easter);

		$diff = $date->diff($easter);

		// @codingStandardsIgnoreLine
		if ($diff->format('%a') === '0') {
			return true;
		}

		return false;
	}

	public function getName(): string
	{
		return 'orthodox.easter';
	}

	protected function modifyDate(DateTime $date): void
	{
		// for additional modification
	}

	private function getEasterDate(DateTimeInterface $date): int
	{
		$year = intval($date->format('Y'));
		$a = $year % 4;
		$b = $year % 7;
		$c = $year % 19;
		$d = (19 * $c + 15) % 30;
		$e = (2 * $a + 4 * $b - $d + 34) % 7;
		$month = intval(floor(($d + $e + 114) / 31));
		$day = (($d + $e + 114) % 31) + 1;

		return mktime(0, 0, 0, $month, $day + 13, $year);
	}
}
