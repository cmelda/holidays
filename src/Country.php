<?php

declare(strict_types=1);

namespace Cmelda\Holidays;

use Cmelda\Holidays\Country\CountrySpecial;
use ReflectionClass;
use ReflectionException;

enum Country: string
{
	case AUSTRIA = 'aut';
	case CROATIA = 'hrv';
	case CZECH = 'cze';
	case DENEMARK = 'dnk';
	case FRANCE = 'fra';
	case FINLAND = 'fin';
	case GERMANY = 'deu';
	case HUNGARY = 'hun';
	case ITALY = 'ita';
	case NORWAY = 'nor';
	case POLISH = 'pol';
	case ROMANIA = 'rou';
	case SLOVAK = 'svk';
	case SLOVENIA = 'svn';
	case SWEDEN = 'swe';

	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return $this->getCountrySpecial()->getSpecialHolidays();
	}

	public function getCountrySpecial(): CountrySpecial
	{
		try {
			$className = 'Cmelda\\Holidays\\Country\\' . ucfirst($this->value);
			$class = new ReflectionClass($className);

			return $class->newInstance();
		} catch (ReflectionException) {
			return new CountrySpecial();
		}
	}
}
