<?php

declare(strict_types=1);

namespace Cmelda\Holidays;

use Cmelda\Holidays\Exceptions\NoNameException;
use Cmelda\Holidays\Exceptions\NoPublicHolidayException;
use DateTimeInterface;
use OutOfBoundsException;

final class Day
{
	/** @var Holidays[] */
	protected array $country;
	protected readonly DateTimeInterface $date;

	public function __construct(DateTimeInterface $date)
	{
		$this->date = $date;
	}

	/**
	 * @return string[]
	 * @throws NoNameException if name is not exists
	 */
	public function getNames(string $iso): array
	{
		return $this->getCountry($iso)
			->getDay($this->date)
			->getNames();
	}

	/**
	 * @return string[]
	 * @throws NoPublicHolidayException if public holiday is not exists
	 */
	public function getPublicHolidays(string $iso): array
	{
		return $this->getCountry($iso)
			->getDay($this->date)
			->getPublicHolidays();
	}

	public function isPublicHoliday(string $iso): bool
	{
		try {
			$this->getPublicHolidays($iso);
		} catch (NoPublicHolidayException) {
			return false;
		}

		return true;
	}

	/**
	 * @throws OutOfBoundsException if country is not exists or implemented
	 */
	protected function getCountry(string $iso): Holidays
	{
		return $this->country[$iso] ??= new Holidays($iso);
	}
}
