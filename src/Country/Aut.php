<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaintsDay;
use Cmelda\Holidays\Special\AssumptionMary;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\Epiphany;
use Cmelda\Holidays\Special\FeastOfCorpusChristi;
use Cmelda\Holidays\Special\FeastOfTheAscension;
use Cmelda\Holidays\Special\ImmaculateConception;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\PentecostMonday;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Aut extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new Epiphany(),
			new EasterMonday(),
			new FeastOfTheAscension(),
			new PentecostMonday(),
			new FeastOfCorpusChristi(),
			new ImmaculateConception(),
			new AllSaintsDay(),
			new WorkersDay(),
			new AssumptionMary(),
			new ChristmasDay(),
			new SaintStephenDay(),
			$this->getNationalDay(),
		];
	}

	public function getNationalDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2610';

			public function getName(): string
			{
				return 'Nationalfeiertag';
			}
		};
	}
}
