<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\Easter;
use Cmelda\Holidays\Special\EasterFriday;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\EasterThursday;
use Cmelda\Holidays\Special\FeastOfTheAscension;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\Pentecost;
use Cmelda\Holidays\Special\PentecostMonday;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Nor extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new EasterThursday(),
			new EasterFriday(),
			new Easter(),
			new EasterMonday(),
			new FeastOfTheAscension(),
			new Pentecost(),
			new PentecostMonday(),
			new ChristmasDay(),
			new SaintStephenDay(),
			new WorkersDay(),
			$this->getConstitutionDay(),
		];
	}

	public function getConstitutionDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1705';

			public function getName(): string
			{
				return 'Grunnlovsdagen';
			}
		};
	}
}
