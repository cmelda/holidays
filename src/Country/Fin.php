<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaints;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\ChristmasEve;
use Cmelda\Holidays\Special\Easter;
use Cmelda\Holidays\Special\EasterFriday;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\Epiphany;
use Cmelda\Holidays\Special\FeastOfTheAscension;
use Cmelda\Holidays\Special\MidsummerDay;
use Cmelda\Holidays\Special\MidsummerEve;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\Pentecost;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Fin extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new Epiphany(),
			new EasterFriday(),
			new Easter(),
			new EasterMonday(),
			new WorkersDay(),
			new Pentecost(),
			new FeastOfTheAscension(),
			new MidsummerEve(),
			new MidsummerDay(),
			new AllSaints(),
			new ChristmasDay(),
			new ChristmasEve(),
			new SaintStephenDay(),
			$this->getIndependenceDay(),
		];
	}

	public function getIndependenceDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0612';

			public function getName(): string
			{
				return 'Itsenäisyyspäivä';
			}
		};
	}
}
