<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaintsDay;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\Easter;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\Epiphany;
use Cmelda\Holidays\Special\FeastOfCorpusChristi;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\PentecostMonday;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Pol extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new Epiphany(),
			new Easter(),
			new EasterMonday(),
			new PentecostMonday(),
			new FeastOfCorpusChristi(),
			new WorkersDay(),
			new AllSaintsDay(),
			new ChristmasDay(),
			new SaintStephenDay(),
			$this->getConstitutionDay(),
			$this->getArmedForceDay(),
			$this->getIndependenceDay(),
		];
	}

	public function getConstitutionDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0305';

			public function getName(): string
			{
				return 'Święto Konstytucji';
			}
		};
	}

	public function getArmedForceDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1508';

			public function getName(): string
			{
				return 'Święto Wojska Polskiego';
			}
		};
	}

	public function getIndependenceDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1111';

			public function getName(): string
			{
				return 'Święto Niepodległości';
			}
		};
	}
}
