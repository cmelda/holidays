<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaintsDay;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\ChristmasEve;
use Cmelda\Holidays\Special\EasterFriday;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\PentecostMonday;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;
use DateTimeImmutable;

class Hun extends CountrySpecial
{
	public function changeDay(DateTimeImmutable $date): DateTimeImmutable
	{
		$year = intval($date->format('Y'));
		if (
			!($year % 400 === 0 || ($year % 4 === 0 && $year % 100 !== 0))
			&& ($date->format('md') >= '0225' && $date->format('md') <= '0228')
		) {
			return $date->modify('+1 day');
		}

		return $date;
	}

	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new EasterFriday(),
			new EasterMonday(),
			new PentecostMonday(),
			new WorkersDay(),
			new ChristmasEve(),
			new ChristmasDay(),
			new SaintStephenDay(),
			new AllSaintsDay(),
			$this->getRevolution1848Day(),
			$this->getNationalDay(),
			$this->getRevolution1956Day(),
		];
	}

	public function getRevolution1848Day(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1503';

			public function getName(): string
			{
				return 'Az 1848-as forradalom ünnepe';
			}
		};
	}

	public function getNationalDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2008';

			public function getName(): string
			{
				return 'Állami ünnep az államalapítás és az államalapító Szent István király emlékére';
			}
		};
	}

	public function getRevolution1956Day(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2310';

			public function getName(): string
			{
				return '1956-os forradalom ünnepe és a harmadik magyar köztársaság kikiáltásának napja';
			}
		};
	}
}
