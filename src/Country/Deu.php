<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\EasterFriday;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\FeastOfTheAscension;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\PentecostMonday;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Deu extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new EasterFriday(),
			new EasterMonday(),
			new FeastOfTheAscension(),
			new PentecostMonday(),
			new WorkersDay(),
			new ChristmasDay(),
			new SaintStephenDay(),
			$this->getUnityDay(),
		];
	}

	public function getUnityDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0310';

			public function getName(): string
			{
				return 'Tag der Deutschen Einheit';
			}
		};
	}
}
