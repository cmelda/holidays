<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaintsDay;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\ChristmasEve;
use Cmelda\Holidays\Special\CyrilMethodius;
use Cmelda\Holidays\Special\EasterFriday;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\Epiphany;
use Cmelda\Holidays\Special\LadyOfSorrows;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Svk extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new Epiphany(),
			new EasterFriday(),
			new EasterMonday(),
			new WorkersDay(),
			new CyrilMethodius(),
			new ChristmasDay(),
			new ChristmasEve(),
			new SaintStephenDay(),
			new LadyOfSorrows(),
			new AllSaintsDay(),
			$this->getEstablishmentDay(),
			$this->getVictoryOverFascismDay(),
			$this->getNationalUprisingDay(),
			$this->getConstitutionDay(),
			$this->getCsEstablishmentDay(),
			$this->getFreedomDay(),
		];
	}

	public function getEstablishmentDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0101';

			public function getName(): string
			{
				return 'Deň vzniku Slovenskej republiky';
			}
		};
	}

	public function getVictoryOverFascismDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0805';

			public function getName(): string
			{
				return 'Deň víťazstva nad fašizmom';
			}
		};
	}

	public function getNationalUprisingDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2908';

			public function getName(): string
			{
				return 'Výročie Slovenského národného povstania';
			}
		};
	}

	public function getConstitutionDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0109';

			public function getName(): string
			{
				return 'Deň Ústavy Slovenskej republiky';
			}
		};
	}

	public function getCsEstablishmentDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2810';

			public function getName(): string
			{
				return 'Deň vzniku samostatného česko-slovenského štátu';
			}
		};
	}

	public function getFreedomDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1711';

			public function getName(): string
			{
				return 'Deň boja za slobodu a demokraciu';
			}
		};
	}
}
