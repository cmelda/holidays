<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use DateTimeImmutable;

class CountrySpecial
{
	public function changeDay(DateTimeImmutable $date): DateTimeImmutable
	{
		return $date;
	}

	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [];
	}
}
