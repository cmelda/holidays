<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaintsDay;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\DormitionOfMotherOfGod;
use Cmelda\Holidays\Special\Easter;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\Epiphany;
use Cmelda\Holidays\Special\FeastOfCorpusChristi;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Hrv extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new Epiphany(),
			new Easter(),
			new EasterMonday(),
			new FeastOfCorpusChristi(),
			new DormitionOfMotherOfGod(),
			new WorkersDay(),
			new ChristmasDay(),
			new SaintStephenDay(),
			new AllSaintsDay(),
			$this->getNationalDay(),
			$this->getAntiFascistDay(),
			$this->getVictoryDay(),
			$this->getDayVictimsHomelandWar(),
		];
	}

	public function getNationalDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '3005';

			public function getName(): string
			{
				return 'Dan državnosti';
			}
		};
	}

	public function getAntiFascistDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2206';

			public function getName(): string
			{
				return 'Dan antifašističke borbe';
			}
		};
	}

	public function getVictoryDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0508';

			public function getName(): string
			{
				return 'Dan pobjede i domovinske zahvalnosti';
			}
		};
	}

	public function getDayVictimsHomelandWar(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1811';

			public function getName(): string
			{
				return 'Dan sjećanja na žrtve Domovinskog rata i Dan sjećanja na žrtvu Vukovara i Škabrnje';
			}
		};
	}
}
