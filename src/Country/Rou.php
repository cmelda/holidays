<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AndrewApostle;
use Cmelda\Holidays\Special\ChildrensDay;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\DormitionOfMotherOfGod;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\NewYearAfter;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\Orthodox\Easter;
use Cmelda\Holidays\Special\Orthodox\EasterFriday;
use Cmelda\Holidays\Special\Orthodox\EasterMonday;
use Cmelda\Holidays\Special\Orthodox\Pentecost;
use Cmelda\Holidays\Special\Orthodox\PentecostMonday;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Rou extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new NewYearAfter(),
			new EasterFriday(),
			new Easter(),
			new EasterMonday(),
			new Pentecost(),
			new PentecostMonday(),
			new WorkersDay(),
			new DormitionOfMotherOfGod(),
			new AndrewApostle(),
			new ChristmasDay(),
			new SaintStephenDay(),
			new ChildrensDay(),
			$this->getUnificationOfRomanianDay(),
			$this->getUnionDay(),
		];
	}

	public function getUnificationOfRomanianDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2401';

			public function getName(): string
			{
				return 'Ziua Unirii Principatelor Române';
			}
		};
	}

	public function getUnionDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0112';

			public function getName(): string
			{
				return 'Ziua Națională a României';
			}
		};
	}
}
