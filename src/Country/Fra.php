<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaintsDay;
use Cmelda\Holidays\Special\AssumptionMary;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\FeastOfTheAscension;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\PentecostMonday;
use Cmelda\Holidays\Special\WorkersDay;

class Fra extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new EasterMonday(),
			new FeastOfTheAscension(),
			new PentecostMonday(),
			new WorkersDay(),
			new ChristmasDay(),
			new AssumptionMary(),
			new AllSaintsDay(),
			$this->getVictoryDay(),
			$this->getBastilleDay(),
			$this->getArmisticeDay(),
		];
	}

	public function getVictoryDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0805';

			public function getName(): string
			{
				return 'Fête de la Victoire 1945';
			}
		};
	}

	public function getBastilleDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1407';

			public function getName(): string
			{
				return 'Fête Nationale';
			}
		};
	}

	public function getArmisticeDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1111';

			public function getName(): string
			{
				return 'Armistice 1918';
			}
		};
	}
}
