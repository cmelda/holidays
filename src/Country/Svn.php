<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaintsDay;
use Cmelda\Holidays\Special\AssumptionMary;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\Easter;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\NewYearAfter;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\Pentecost;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;
use Cmelda\Holidays\Special\WorkersDayAfter;

class Svn extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new NewYearAfter(),
			new Easter(),
			new EasterMonday(),
			new Pentecost(),
			new WorkersDay(),
			new WorkersDayAfter(),
			new AssumptionMary(),
			new AllSaintsDay(),
			new ChristmasDay(),
			new SaintStephenDay(),
			$this->getPreserenDay(),
			$this->getRebellionOccupierDay(),
			$this->getStatehoodDay(),
			$this->getReformationDay(),
		];
	}

	public function getPreserenDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0802';

			public function getName(): string
			{
				return 'Prešernov dan';
			}
		};
	}

	public function getRebellionOccupierDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2704';

			public function getName(): string
			{
				return 'Dan upora proti okupatorju';
			}
		};
	}

	public function getStatehoodDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2506';

			public function getName(): string
			{
				return 'Dan državnosti';
			}
		};
	}

	public function getReformationDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '3110';

			public function getName(): string
			{
				return 'Dan reformacije';
			}
		};
	}
}
