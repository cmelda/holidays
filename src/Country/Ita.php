<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\AllSaintsDay;
use Cmelda\Holidays\Special\AssumptionMary;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\Epiphany;
use Cmelda\Holidays\Special\ImmaculateConception;
use Cmelda\Holidays\Special\NewYear;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Ita extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new NewYear(),
			new Epiphany(),
			new EasterMonday(),
			new WorkersDay(),
			new ChristmasDay(),
			new SaintStephenDay(),
			new AssumptionMary(),
			new AllSaintsDay(),
			new ImmaculateConception(),
			$this->getLiberationDay(),
			$this->getRepublicaDay(),
		];
	}

	public function getLiberationDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2504';

			public function getName(): string
			{
				return 'Festa della Liberazione';
			}
		};
	}

	public function getRepublicaDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0206';

			public function getName(): string
			{
				return 'Festa della Repubblica';
			}
		};
	}
}
