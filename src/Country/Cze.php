<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Country;

use Cmelda\Holidays\Special;
use Cmelda\Holidays\Special\ChristmasDay;
use Cmelda\Holidays\Special\ChristmasEve;
use Cmelda\Holidays\Special\CyrilMethodius;
use Cmelda\Holidays\Special\EasterFriday;
use Cmelda\Holidays\Special\EasterMonday;
use Cmelda\Holidays\Special\OneDaySpecial;
use Cmelda\Holidays\Special\SaintStephenDay;
use Cmelda\Holidays\Special\WorkersDay;

class Cze extends CountrySpecial
{
	/**
	 * @return Special[]
	 */
	public function getSpecialHolidays(): array
	{
		return [
			new EasterFriday(),
			new EasterMonday(),
			new WorkersDay(),
			new ChristmasDay(),
			new ChristmasEve(),
			new SaintStephenDay(),
			new CyrilMethodius(),
			$this->getRestorationDay(),
			$this->getVictoryDay(),
			$this->getHusDay(),
			$this->getNationalDay(),
			$this->getCountryDay(),
			$this->getDemocracyDay(),
		];
	}

	public function getRestorationDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0101';

			public function getName(): string
			{
				return 'Den obnovy samostatného českého státu';
			}
		};
	}

	public function getVictoryDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0805';

			public function getName(): string
			{
				return 'Den vítězství';
			}
		};
	}

	public function getHusDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '0607';

			public function getName(): string
			{
				return 'Den upálení mistra Jana Husa';
			}
		};
	}

	public function getNationalDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2809';

			public function getName(): string
			{
				return 'Den české státnosti';
			}
		};
	}

	public function getCountryDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '2810';

			public function getName(): string
			{
				return 'Den vzniku Československa';
			}
		};
	}

	public function getDemocracyDay(): OneDaySpecial
	{
		return new class extends OneDaySpecial {
			protected string $day = '1711';

			public function getName(): string
			{
				return 'Den boje za svobodu a demokracii';
			}
		};
	}
}
