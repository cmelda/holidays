<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Exceptions;

class NoNameException extends HolidayException
{
	protected string $exceptionMessage = 'No name-day in "%s".';
}
