<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Exceptions;

use DateTimeInterface;
use LogicException;

abstract class HolidayException extends LogicException
{
	protected string $exceptionMessage = '';

	public function __construct(DateTimeInterface $date)
	{
		$message = sprintf($this->exceptionMessage, $date->format('j.n.'));

		parent::__construct($message);
	}
}
