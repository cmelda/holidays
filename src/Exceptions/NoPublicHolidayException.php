<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Exceptions;

class NoPublicHolidayException extends HolidayException
{
	protected string $exceptionMessage = 'The date "%s" is not public holiday';
}
