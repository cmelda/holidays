# Changelog

## [1.4] - 2025-01-05
 - Separate days and public holidays, and small refactoring
 - Fix Easter (Romanian)
 - Remove required php-calendar extension

### Add country
- Norway

## [1.3] - 2024-08-29

### Add country
- Denmark
- France
- Finland
- Sweden

## [1.2] - 2024-08-27

### Add country
- Croatia
- Italy
- Romania
- Slovenia

## [1.0] - 2024-06-25

### Create library with country
- Austria
- Czechia
- Germany
- Hungary
- Poland
- Slovakia