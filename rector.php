<?php

use Rector\Config\RectorConfig;
use Rector\Php80\Rector\Class_\ClassPropertyAssignToConstructorPromotionRector;
use Rector\Set\ValueObject\SetList;
use Rector\ValueObject\PhpVersion;

return RectorConfig::configure()
	->withPaths([
		__DIR__ . '/src',
		__DIR__ . '/tests/Unit',
	])
	->withSets([SetList::DEAD_CODE])
	->withAttributesSets(doctrine: true)
    ->withImportNames()
	->withPhpSets(PhpVersion::PHP_83)
	->withSkip([
		ClassPropertyAssignToConstructorPromotionRector::class,
	]);
    