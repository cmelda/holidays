# Holidays

[![Latest Stable Version](https://poser.pugx.org/cmelda/holidays/version.png)](https://packagist.org/packages/cmelda/holidays) [![Total Downloads](https://poser.pugx.org/cmelda/holidays/downloads.png)](https://packagist.org/packages/cmelda/holidays) [![License](https://poser.pugx.org/cmelda/holidays/license.svg)](https://packagist.org/packages/cmelda/holidays) [![PHP Version Require](http://poser.pugx.org/cmelda/holidays/require/php)](https://packagist.org/packages/cmelda/holidays)

## Install

```
composer require cmelda/holidays
```

## List of available countries

[List](https://gitlab.com/cmelda/holidays/-/blob/main/List.md)

## Usage

### Public Holiday

```php

$holidays = new \Cmelda\Holidays\Holidays('cze');

$date = new DateTimeImmutable();

$day = $holidays->getDay($date);

try {
    $publicHolidays = $day->getPublicHoliday();
} catch (\Cmelda\Holidays\Exceptions\NoPublicHolidayException $e) {
    // no public holiday
}

```

### Name's day

```php

$holidays = new \Cmelda\Holidays\Holidays('cze');

$date = new DateTimeImmutable();

$day = $holidays->getDay($date);

try {
    $names = $day->getNames();
} catch (\Cmelda\Holidays\Exceptions\NoNameException $e) {
    // no name's day
}

```
### Day 

```php

$date = new DateTimeImmutable();

$day = new \Cmelda\Holidays\Day($date);

try {
    $names = $day->getNames('cze');
} catch (\Cmelda\Holidays\Exceptions\NoNameException $e) {
    // no name's day
}

try {
    $names = $day->getNames('svk');
} catch (\Cmelda\Holidays\Exceptions\NoNameException $e) {
    // no name's day
}

```


## License

Distributed under the MIT License. See [`LICENSE.txt`](LICENSE.txt) for more information.

## Donation

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/G2G116PSZC)



