<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class DnkTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'0101',
		'0601',
		'2501',
		'0202',
		'2202',
		'2902',
		'0903',
		'2503',
		'0305',
		'2206',
		'2706',
		'1507',
		'0108',
		'0608',
		'1409',
		'0111',
		'0211',
		'2512',
		'2812',
	];
	protected string $iso = 'dnk';
	protected int $publicHolidaysCount = 10;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Nytårsdag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Nytårsdag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Nytårsdag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-14');
		$this->assertContains('Skærtorsdag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-6');
		$this->assertContains('Skærtorsdag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-28');
		$this->assertContains('Skærtorsdag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-15');
		$this->assertContains('Langfredag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-7');
		$this->assertContains('Langfredag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-29');
		$this->assertContains('Langfredag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-17');
		$this->assertContains('Påskedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-9');
		$this->assertContains('Påskedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-31');
		$this->assertContains('Påskedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('2. påskedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('2. påskedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('2. påskedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-26');
		$this->assertContains('Kristi Himmelfartsdag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-18');
		$this->assertContains('Kristi Himmelfartsdag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-9');
		$this->assertContains('Kristi Himmelfartsdag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-6-5');
		$this->assertContains('Pinsedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-28');
		$this->assertContains('Pinsedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-19');
		$this->assertContains('Pinsedag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-6-6');
		$this->assertContains('2. pinsedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-29');
		$this->assertContains('2. pinsedag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-20');
		$this->assertContains('2. pinsedag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('1. juledag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('1. juledag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('1. juledag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('2. juledag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('2. juledag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('2. juledag', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
