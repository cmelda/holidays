<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class FinTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'0101',
		'2902',
		'2512',
	];
	protected string $iso = 'fin';
	protected int $publicHolidaysCount = 15;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Uudenvuodenpäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Uudenvuodenpäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Uudenvuodenpäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-1-6');
		$this->assertContains('Loppiainen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-6');
		$this->assertContains('Loppiainen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-6');
		$this->assertContains('Loppiainen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-15');
		$this->assertContains('Pitkäperjantai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-7');
		$this->assertContains('Pitkäperjantai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-29');
		$this->assertContains('Pitkäperjantai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-17');
		$this->assertContains('Pääsiäinen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-9');
		$this->assertContains('Pääsiäinen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-31');
		$this->assertContains('Pääsiäinen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('2. pääsiäispäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('2. pääsiäispäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('2. pääsiäispäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Vappu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Vappu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Vappu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-26');
		$this->assertContains('Helatorstai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-18');
		$this->assertContains('Helatorstai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-9');
		$this->assertContains('Helatorstai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-6-5');
		$this->assertContains('Helluntai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-28');
		$this->assertContains('Helluntai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-19');
		$this->assertContains('Helluntai', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-6-24');
		$this->assertContains('Juhannusaatto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-23');
		$this->assertContains('Juhannusaatto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-6-21');
		$this->assertContains('Juhannusaatto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-6-25');
		$this->assertContains('Juhannuspäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-24');
		$this->assertContains('Juhannuspäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-6-22');
		$this->assertContains('Juhannuspäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-11-5');
		$this->assertContains('Pyhäinpäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-4');
		$this->assertContains('Pyhäinpäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-2');
		$this->assertContains('Pyhäinpäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-6');
		$this->assertContains('Itsenäisyyspäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-6');
		$this->assertContains('Itsenäisyyspäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-6');
		$this->assertContains('Itsenäisyyspäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-24');
		$this->assertContains('Jouluaatto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-24');
		$this->assertContains('Jouluaatto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-24');
		$this->assertContains('Jouluaatto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Joulupäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Joulupäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Joulupäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('2. joulupäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('2. joulupäivä', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('2. joulupäivä', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
