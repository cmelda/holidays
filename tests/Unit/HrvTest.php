<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class HrvTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'2502',
		'2902',
		'0504',
		'2105',
		'3105',
		'1307',
		'0608',
		'1811',
		'0812',
	];
	protected string $iso = 'hrv';
	protected int $publicHolidaysCount = 14;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Nova godina', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Nova godina', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Nova godina', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-1-6');
		$this->assertContains('Sveta tri kralja', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-6');
		$this->assertContains('Sveta tri kralja', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-6');
		$this->assertContains('Sveta tri kralja', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-17');
		$this->assertContains('Uskrs', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-9');
		$this->assertContains('Uskrs', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-31');
		$this->assertContains('Uskrs', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('Uskrsni ponedjeljak', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('Uskrsni ponedjeljak', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('Uskrsni ponedjeljak', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Praznik rada', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Praznik rada', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Praznik rada', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-30');
		$this->assertContains('Dan državnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-30');
		$this->assertContains('Dan državnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-30');
		var_dump("test -----------------------------------------------------------------------------------");
		var_dump($this->holidays->getDay($date)->getPublicHolidays());
		$this->assertContains('Dan državnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-6-16');
		$this->assertContains('Tijelovo', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-8');
		$this->assertContains('Tijelovo', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-30');
		$this->assertContains('Tijelovo', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-6-22');
		$this->assertContains('Dan antifašističke borbe', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-22');
		$this->assertContains('Dan antifašističke borbe', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-6-22');
		$this->assertContains('Dan antifašističke borbe', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-8-5');
		$this->assertContains('Dan pobjede i domovinske zahvalnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-8-5');
		$this->assertContains('Dan pobjede i domovinske zahvalnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-8-5');
		$this->assertContains('Dan pobjede i domovinske zahvalnosti', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-8-15');
		$this->assertContains('Velika Gospa', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-8-15');
		$this->assertContains('Velika Gospa', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-8-15');
		$this->assertContains('Velika Gospa', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-11-1');
		$this->assertContains('Dan svih svetih', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-1');
		$this->assertContains('Dan svih svetih', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-1');
		$this->assertContains('Dan svih svetih', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-11-18');
		$this->assertContains(
			'Dan sjećanja na žrtve Domovinskog rata i Dan sjećanja na žrtvu Vukovara i Škabrnje',
			$this->holidays->getDay($date)->getPublicHolidays(),
		);

		$date = new DateTimeImmutable('2023-11-18');
		$this->assertContains(
			'Dan sjećanja na žrtve Domovinskog rata i Dan sjećanja na žrtvu Vukovara i Škabrnje',
			$this->holidays->getDay($date)->getPublicHolidays(),
		);

		$date = new DateTimeImmutable('2024-11-18');
		$this->assertContains(
			'Dan sjećanja na žrtve Domovinskog rata i Dan sjećanja na žrtvu Vukovara i Škabrnje',
			$this->holidays->getDay($date)->getPublicHolidays(),
		);

		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Božić', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Božić', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Božić', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('Sveti Stjepan', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('Sveti Stjepan', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('Sveti Stjepan', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
