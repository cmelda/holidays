<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class DeuTest extends BaseTest
{
	protected string $iso = 'deu';
	protected int $publicHolidaysCount = 9;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Neujahrstag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Neujahrstag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Neujahrstag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-15');
		$this->assertContains('Karfreitag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-7');
		$this->assertContains('Karfreitag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-29');
		$this->assertContains('Karfreitag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('Ostermontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('Ostermontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('Ostermontag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Tag der Arbeit', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Tag der Arbeit', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Tag der Arbeit', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-5-26');
		$this->assertContains('Christi Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-18');
		$this->assertContains('Christi Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-9');
		$this->assertContains('Christi Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-6-6');
		$this->assertContains('Pfingstmontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-29');
		$this->assertContains('Pfingstmontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-20');
		$this->assertContains('Pfingstmontag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-10-3');
		$this->assertContains('Tag der Deutschen Einheit', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-10-3');
		$this->assertContains('Tag der Deutschen Einheit', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-10-3');
		$this->assertContains('Tag der Deutschen Einheit', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Erster Weihnachtsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Erster Weihnachtsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Erster Weihnachtsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('Zweiter Weihnachtsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('Zweiter Weihnachtsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('Zweiter Weihnachtsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
