<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class AutTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
	];
	protected string $iso = 'aut';
	protected int $publicHolidaysCount = 13;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Neujahrstag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Neujahrstag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Neujahrstag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-1-6');
		$this->assertContains('Heilige Drei Könige', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-6');
		$this->assertContains('Heilige Drei Könige', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-6');
		$this->assertContains('Heilige Drei Könige', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('Ostermontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('Ostermontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('Ostermontag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Staatsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Staatsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Staatsfeiertag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-5-26');
		$this->assertContains('Christi Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-18');
		$this->assertContains('Christi Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-9');
		$this->assertContains('Christi Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-6-6');
		$this->assertContains('Pfingstmontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-29');
		$this->assertContains('Pfingstmontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-20');
		$this->assertContains('Pfingstmontag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-6-16');
		$this->assertContains('Fronleichnam', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-8');
		$this->assertContains('Fronleichnam', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-30');
		$this->assertContains('Fronleichnam', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-8-15');
		$this->assertContains('Mariä Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-8-15');
		$this->assertContains('Mariä Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-8-15');
		$this->assertContains('Mariä Himmelfahrt', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-10-26');
		$this->assertContains('Nationalfeiertag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-10-26');
		$this->assertContains('Nationalfeiertag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-10-26');
		$this->assertContains('Nationalfeiertag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-11-1');
		$this->assertContains('Allerheiligen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-1');
		$this->assertContains('Allerheiligen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-1');
		$this->assertContains('Allerheiligen', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-8');
		$this->assertContains('Mariä Empfängnis', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-8');
		$this->assertContains('Mariä Empfängnis', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-8');
		$this->assertContains('Mariä Empfängnis', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Christtag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Christtag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Christtag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('Stefanitag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('Stefanitag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('Stefanitag', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
