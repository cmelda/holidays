<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class CzeTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'0101',
		'2512',
	];
	protected string $iso = 'cze';
	protected int $publicHolidaysCount = 13;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Den obnovy samostatného českého státu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Den obnovy samostatného českého státu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Den obnovy samostatného českého státu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-15');
		$this->assertContains('Velký pátek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-7');
		$this->assertContains('Velký pátek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-29');
		$this->assertContains('Velký pátek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('Velikonoční pondělí', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('Velikonoční pondělí', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('Velikonoční pondělí', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Svátek práce', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Svátek práce', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Svátek práce', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-7-5');
		$this->assertContains('Den věrozvěstů Cyrila a Metoděje', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-7-5');
		$this->assertContains('Den věrozvěstů Cyrila a Metoděje', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-7-5');
		$this->assertContains('Den věrozvěstů Cyrila a Metoděje', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-7-6');
		$this->assertContains('Den upálení mistra Jana Husa', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-7-6');
		$this->assertContains('Den upálení mistra Jana Husa', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-7-6');
		$this->assertContains('Den upálení mistra Jana Husa', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-9-28');
		$this->assertContains('Den české státnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-9-28');
		$this->assertContains('Den české státnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-9-28');
		$this->assertContains('Den české státnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-10-28');
		$this->assertContains('Den vzniku Československa', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-10-28');
		$this->assertContains('Den vzniku Československa', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-10-28');
		$this->assertContains('Den vzniku Československa', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-11-17');
		$this->assertContains('Den boje za svobodu a demokracii', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-17');
		$this->assertContains('Den boje za svobodu a demokracii', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-17');
		$this->assertContains('Den boje za svobodu a demokracii', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-24');
		$this->assertContains('Štědrý den', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-24');
		$this->assertContains('Štědrý den', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-24');
		$this->assertContains('Štědrý den', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('1. svátek vánoční', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('1. svátek vánoční', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('1. svátek vánoční', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('2. svátek vánoční', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('2. svátek vánoční', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('2. svátek vánoční', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
