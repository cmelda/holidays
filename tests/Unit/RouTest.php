<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class RouTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'0601',
		'0202',
		'0603',
		'0903',
		'2003',
		'2503',
		'2904',
		'0705',
		'1007',
		'0108',
		'0408',
		'0608',
		'1508',
		'1608',
		'3108',
		'0809',
		'1409',
		'0110',
		'2110',
		'0711',
		'2111',
		'2312',
		'2512',
		'2612',
		'2812',
		'2912',
	];
	protected string $iso = 'rou';
	protected int $publicHolidaysCount = 15;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Anul Nou', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Anul Nou', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Anul Nou', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-1-2');
		$this->assertContains('Anul Nou', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-2');
		$this->assertContains('Anul Nou', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-2');
		$this->assertContains('Anul Nou', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-1-24');
		$this->assertContains('Ziua Unirii Principatelor Române', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-24');
		$this->assertContains('Ziua Unirii Principatelor Române', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-24');
		$this->assertContains('Ziua Unirii Principatelor Române', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-22');
		$this->assertContains('Vinerea Mare', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-14');
		$this->assertContains('Vinerea Mare', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-3');
		$this->assertContains('Vinerea Mare', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-24');
		$this->assertContains('Duminica Paștelui', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-16');
		$this->assertContains('Duminica Paștelui', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-5');
		$this->assertContains('Duminica Paștelui', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-25');
		$this->assertContains('Lunea Luminată', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-17');
		$this->assertContains('Lunea Luminată', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-6');
		$this->assertContains('Lunea Luminată', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Ziua Internațională a Muncii', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Ziua Internațională a Muncii', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Ziua Internațională a Muncii', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-6-1');
		$this->assertContains('Ziua Copilului', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-1');
		$this->assertContains('Ziua Copilului', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-6-1');
		$this->assertContains('Ziua Copilului', $this->holidays->getDay($date)->getPublicHolidays());
				
		$date = new DateTimeImmutable('2022-6-12');
		$this->assertContains('Rusaliile', $this->holidays->getDay($date)->getPublicHolidays());
				
		$date = new DateTimeImmutable('2022-6-13');
		$this->assertContains('Rusaliile', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-4');
		$this->assertContains('Rusaliile', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-5');
		$this->assertContains('Rusaliile', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-6-23');
		$this->assertContains('Rusaliile', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-6-24');
		$this->assertContains('Rusaliile', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-8-15');
		$this->assertContains('Adormirea Maicii Domnului', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-8-15');
		$this->assertContains('Adormirea Maicii Domnului', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-8-15');
		$this->assertContains('Adormirea Maicii Domnului', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-11-30');
		$this->assertContains('Sfântul Andrei', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-30');
		$this->assertContains('Sfântul Andrei', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-30');
		$this->assertContains('Sfântul Andrei', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-12-1');
		$this->assertContains('Ziua Națională a României', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-1');
		$this->assertContains('Ziua Națională a României', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-1');
		$this->assertContains('Ziua Națională a României', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Crăciunul', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Crăciunul', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Crăciunul', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('Crăciunul', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('Crăciunul', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('Crăciunul', $this->holidays->getDay($date)->getPublicHolidays());
	}
}