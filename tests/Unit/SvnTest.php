<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class SvnTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'2501',
		'2901',
	];
	protected string $iso = 'svn';
	protected int $publicHolidaysCount = 15;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Novo leto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Novo leto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Novo leto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-1-2');
		$this->assertContains('Novo leto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-2');
		$this->assertContains('Novo leto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-2');
		$this->assertContains('Novo leto', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-2-8');
		$this->assertContains('Prešernov dan', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-2-8');
		$this->assertContains('Prešernov dan', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-2-8');
		$this->assertContains('Prešernov dan', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-17');
		$this->assertContains('velikonočna nedelja', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-9');
		$this->assertContains('velikonočna nedelja', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-31');
		$this->assertContains('velikonočna nedelja', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('velikonočni ponedeljek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('velikonočni ponedeljek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('velikonočni ponedeljek', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-4-27');
		$this->assertContains('Dan upora proti okupatorju', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-27');
		$this->assertContains('Dan upora proti okupatorju', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-27');
		$this->assertContains('Dan upora proti okupatorju', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Praznik dela', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Praznik dela', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Praznik dela', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-2');
		$this->assertContains('Praznik dela', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-2');
		$this->assertContains('Praznik dela', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-2');
		$this->assertContains('Praznik dela', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-6-5');
		$this->assertContains('binkoštna nedelja', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-28');
		$this->assertContains('binkoštna nedelja', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-19');
		$this->assertContains('binkoštna nedelja', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-6-25');
		$this->assertContains('Dan državnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-25');
		$this->assertContains('Dan državnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-6-25');
		$this->assertContains('Dan državnosti', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-8-15');
		$this->assertContains('Marijino vnebovzetje', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-8-15');
		$this->assertContains('Marijino vnebovzetje', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-8-15');
		$this->assertContains('Marijino vnebovzetje', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-10-31');
		$this->assertContains('Dan reformacije', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-10-31');
		$this->assertContains('Dan reformacije', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-10-31');
		$this->assertContains('Dan reformacije', $this->holidays->getDay($date)->getPublicHolidays());
	
		$date = new DateTimeImmutable('2022-11-1');
		$this->assertContains('Dan spomina na mrtve', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-1');
		$this->assertContains('Dan spomina na mrtve', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-1');
		$this->assertContains('Dan spomina na mrtve', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Božič', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Božič', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Božič', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('Dan samostojnosti in enotnosti', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('Dan samostojnosti in enotnosti', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('Dan samostojnosti in enotnosti', $this->holidays->getDay($date)->getPublicHolidays());
	}
}