<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Codeception\Test\Unit;
use Cmelda\Holidays\Holidays;
use Cmelda\Holidays\Exceptions\NoNameException;
use DateTime;

abstract class BaseTest extends Unit
{
    protected Holidays $holidays;
	protected string $iso;
	protected int $publicHolidaysCount;

	/** @var string[] */
	protected array $emptyDates = [];

	public function _before(): void
	{
		$this->holidays = new Holidays($this->iso);
	}

	public function testNonEmptyNames(): void
	{
		$publicHoliday = [];
		$date = new DateTime('2024-01-01');
		do {
			$day = $this->holidays->getDay($date);
			if (in_array($date->format('dm'), $this->emptyDates, true)) {
				try {
					$this->assertEmpty(
						$day->getNames(),
						sprintf('Date %s contains names.', $date->format('j.n.Y')),
					);
				} catch (NoNameException) {
				}
			} else {
				$names = $day->getNames();
				$this->assertNotEmpty(
					$names,
					sprintf('Date %s doesn\'t contains nothing.', $date->format('j.n.Y')),
				);
				foreach ($names as $name) {
					$this->assertMatchesRegularExpression(
						'/^(\p{Lu}\p{Ll}+(\-\p{Lu}\p{Ll}+)*)|(\p{Lo})+$/u',
						$name,
						sprintf('Date %s contains non acceptable char in name.', $date->format('j.n.Y')),
					);
				}
			}

			if ($day->isPublicHoliday()) {
				foreach ($day->getPublicHolidays() as $oneHoliday) {
					$publicHoliday[] = $date->format('j.n.');
				}
			}

			$date->modify('+ 1 day');
		} while ($date->format('Y') !== '2025');

		$this->assertSame(
			count($publicHoliday),
			$this->publicHolidaysCount,
			sprintf('Count of public holidays is not same. In data are these public holidays: %s', implode(', ', $publicHoliday))
		);
	}

	abstract public function testPublicHolidays() :void;
}
