<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class SweTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'0101',
		'0202',
		'2902',
		'2503',
		'2406',
		'0111',
		'2512',
		'3112',
	];
	protected string $iso = 'swe';
	protected int $publicHolidaysCount = 12;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Nyårsdagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Nyårsdagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Nyårsdagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-1-6');
		$this->assertContains('Trettondedag jul', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-6');
		$this->assertContains('Trettondedag jul', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-6');
		$this->assertContains('Trettondedag jul', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-15');
		$this->assertContains('Långfredagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-7');
		$this->assertContains('Långfredagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-29');
		$this->assertContains('Långfredagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('Annandag påsk', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('Annandag påsk', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('Annandag påsk', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Första Maj', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Första Maj', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Första Maj', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-26');
		$this->assertContains('Kristi himmelsfärds dag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-18');
		$this->assertContains('Kristi himmelsfärds dag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-9');
		$this->assertContains('Kristi himmelsfärds dag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-6-6');
		$this->assertContains('Sveriges nationaldag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-6-6');
		$this->assertContains('Sveriges nationaldag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-6-6');
		$this->assertContains('Sveriges nationaldag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-11-5');
		$this->assertContains('Alla helgons dag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-4');
		$this->assertContains('Alla helgons dag', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-2');
		$this->assertContains('Alla helgons dag', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Juldagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Juldagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Juldagen', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('Annandag jul', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('Annandag jul', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('Annandag jul', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
