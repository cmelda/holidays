<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class FraTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'0101',
		'2501',
		'0608',
		'1409',
		'0111',
		'0812',
	];
	protected string $iso = 'fra';
	protected int $publicHolidaysCount = 11;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Jour de l\'An', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Jour de l\'An', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Jour de l\'An', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('Lundi de Pâques', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('Lundi de Pâques', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('Lundi de Pâques', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Fête du Travail', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Fête du Travail', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Fête du Travail', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-8');
		$this->assertContains('Fête de la Victoire 1945', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-8');
		$this->assertContains('Fête de la Victoire 1945', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-8');
		$this->assertContains('Fête de la Victoire 1945', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-5-26');
		$this->assertContains('Ascension', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-18');
		$this->assertContains('Ascension', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-9');
		$this->assertContains('Ascension', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-6-6');
		$this->assertContains('Lundi de Pentecôte', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-29');
		$this->assertContains('Lundi de Pentecôte', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-20');
		$this->assertContains('Lundi de Pentecôte', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-7-14');
		$this->assertContains('Fête Nationale', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-7-14');
		$this->assertContains('Fête Nationale', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-7-14');
		$this->assertContains('Fête Nationale', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-8-15');
		$this->assertContains('Assomption', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-8-15');
		$this->assertContains('Assomption', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-8-15');
		$this->assertContains('Assomption', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-11-1');
		$this->assertContains('Toussaint', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-1');
		$this->assertContains('Toussaint', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-1');
		$this->assertContains('Toussaint', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-11-11');
		$this->assertContains('Armistice 1918', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-11');
		$this->assertContains('Armistice 1918', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-11');
		$this->assertContains('Armistice 1918', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Noël', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Noël', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Noël', $this->holidays->getDay($date)->getPublicHolidays());
    }
}
