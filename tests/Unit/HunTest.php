<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Exceptions\NoNameException;
use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class HunTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'2502'
	];
	protected string $iso = 'hun';
	protected int $publicHolidaysCount = 12;

	public function testPublicHolidays(): void
	{
		try {
			$date = new DateTimeImmutable('2024-2-25');
			$this->assertNotContains('Géza', $this->holidays->getDay($date)->getNames());
		} catch (NoNameException) {
		}

		$date = new DateTimeImmutable('2023-2-25');
		$this->assertContains('Géza', $this->holidays->getDay($date)->getNames());


		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Újév', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Újév', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Újév', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-3-15');
		$this->assertContains('Az 1848-as forradalom ünnepe', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-3-15');
		$this->assertContains('Az 1848-as forradalom ünnepe', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-15');
		$this->assertContains('Az 1848-as forradalom ünnepe', $this->holidays->getDay($date)->getPublicHolidays());		

		$date = new DateTimeImmutable('2022-4-15');
		$this->assertContains('Nagypéntek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-7');
		$this->assertContains('Nagypéntek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-29');
		$this->assertContains('Nagypéntek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('Húsvét', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('Húsvét', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('Húsvét', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('A munka ünnepe', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('A munka ünnepe', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('A munka ünnepe', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-6-6');
		$this->assertContains('Pünkösd', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-29');
		$this->assertContains('Pünkösd', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-20');
		$this->assertContains('Pünkösd', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-8-20');
		$this->assertContains(
			'Állami ünnep az államalapítás és az államalapító Szent István király emlékére',
			$this->holidays->getDay($date)->getPublicHolidays()
		);

		$date = new DateTimeImmutable('2023-8-20');
		$this->assertContains(
			'Állami ünnep az államalapítás és az államalapító Szent István király emlékére',
			$this->holidays->getDay($date)->getPublicHolidays()
		);

		$date = new DateTimeImmutable('2024-8-20');
		$this->assertContains(
			'Állami ünnep az államalapítás és az államalapító Szent István király emlékére',
			$this->holidays->getDay($date)->getPublicHolidays()
		);
		
		$date = new DateTimeImmutable('2022-10-23');
		$this->assertContains(
			'1956-os forradalom ünnepe és a harmadik magyar köztársaság kikiáltásának napja',
			$this->holidays->getDay($date)->getPublicHolidays()
		);

		$date = new DateTimeImmutable('2023-10-23');
		$this->assertContains(
			'1956-os forradalom ünnepe és a harmadik magyar köztársaság kikiáltásának napja',
			$this->holidays->getDay($date)->getPublicHolidays()
		);

		$date = new DateTimeImmutable('2024-10-23');
		$this->assertContains(
			'1956-os forradalom ünnepe és a harmadik magyar köztársaság kikiáltásának napja',
			$this->holidays->getDay($date)->getPublicHolidays()
		);

		$date = new DateTimeImmutable('2022-11-1');
		$this->assertContains('Mindenszentek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-1');
		$this->assertContains('Mindenszentek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-1');
		$this->assertContains('Mindenszentek', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-24');
		$this->assertContains('Szenteste', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-24');
		$this->assertContains('Szenteste', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-24');
		$this->assertContains('Szenteste', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Karácsony', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Karácsony', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Karácsony', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('Karácsony', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('Karácsony', $this->holidays->getDay($date)->getPublicHolidays());
		
		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('Karácsony', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
