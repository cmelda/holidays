<?php

declare(strict_types=1);

namespace Cmelda\Holidays\Tests\Unit;

use Cmelda\Holidays\Holidays;
use DateTimeImmutable;

class SvkTest extends BaseTest
{
	/** @var string[] */
	protected array $emptyDates = [
		'0101',
		'0105',
		'0211',
		'2512',
	];
	protected string $iso = 'svk';
	protected int $publicHolidaysCount = 16;

	public function testPublicHolidays(): void
	{
		$date = new DateTimeImmutable('2022-1-1');
		$this->assertContains('Deň vzniku Slovenskej republiky', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-1');
		$this->assertContains('Deň vzniku Slovenskej republiky', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-1');
		$this->assertContains('Deň vzniku Slovenskej republiky', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-1-6');
		$this->assertContains('Zjavenie Pána', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-1-6');
		$this->assertContains('Zjavenie Pána', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-1-6');
		$this->assertContains('Zjavenie Pána', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-15');
		$this->assertContains('Veľký piatok', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-7');
		$this->assertContains('Veľký piatok', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-3-29');
		$this->assertContains('Veľký piatok', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-4-18');
		$this->assertContains('Veľkonočný pondelok', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-4-10');
		$this->assertContains('Veľkonočný pondelok', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-4-1');
		$this->assertContains('Veľkonočný pondelok', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-1');
		$this->assertContains('Sviatok práce', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-1');
		$this->assertContains('Sviatok práce', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-1');
		$this->assertContains('Sviatok práce', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-5-8');
		$this->assertContains('Deň víťazstva nad fašizmom', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-5-8');
		$this->assertContains('Deň víťazstva nad fašizmom', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-5-8');
		$this->assertContains('Deň víťazstva nad fašizmom', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-7-5');
		$this->assertContains('Sviatok svätého Cyrila a Metoda', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-7-5');
		$this->assertContains('Sviatok svätého Cyrila a Metoda', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-7-5');
		$this->assertContains('Sviatok svätého Cyrila a Metoda', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-8-29');
		$this->assertContains('Výročie Slovenského národného povstania', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-8-29');
		$this->assertContains('Výročie Slovenského národného povstania', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-8-29');
		$this->assertContains('Výročie Slovenského národného povstania', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-9-1');
		$this->assertContains('Deň Ústavy Slovenskej republiky', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-9-1');
		$this->assertContains('Deň Ústavy Slovenskej republiky', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-9-1');
		$this->assertContains('Deň Ústavy Slovenskej republiky', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-9-15');
		$this->assertContains('Sedembolestná Panna Mária', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-9-15');
		$this->assertContains('Sedembolestná Panna Mária', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-9-15');
		$this->assertContains('Sedembolestná Panna Mária', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-10-28');
		$this->assertContains('Deň vzniku samostatného česko-slovenského štátu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-10-28');
		$this->assertContains('Deň vzniku samostatného česko-slovenského štátu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-10-28');
		$this->assertContains('Deň vzniku samostatného česko-slovenského štátu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-11-1');
		$this->assertContains('Sviatok Všetkých svätých', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-1');
		$this->assertContains('Sviatok Všetkých svätých', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-1');
		$this->assertContains('Sviatok Všetkých svätých', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-11-17');
		$this->assertContains('Deň boja za slobodu a demokraciu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-11-17');
		$this->assertContains('Deň boja za slobodu a demokraciu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-11-17');
		$this->assertContains('Deň boja za slobodu a demokraciu', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-24');
		$this->assertContains('Štedrý deň', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-24');
		$this->assertContains('Štedrý deň', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-24');
		$this->assertContains('Štedrý deň', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-25');
		$this->assertContains('Prvý sviatok vianočný', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-25');
		$this->assertContains('Prvý sviatok vianočný', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-25');
		$this->assertContains('Prvý sviatok vianočný', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2022-12-26');
		$this->assertContains('Druhý sviatok vianočný', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2023-12-26');
		$this->assertContains('Druhý sviatok vianočný', $this->holidays->getDay($date)->getPublicHolidays());

		$date = new DateTimeImmutable('2024-12-26');
		$this->assertContains('Druhý sviatok vianočný', $this->holidays->getDay($date)->getPublicHolidays());
	}
}
